#include "header.h"
#include <stdio.h>

#define IF_NAME "inf000"
extern void get_ip_address_index(struct ifreq *if_idx, char *interface);
extern void get_mac_address(struct ifreq * if_mac, char *interface);
extern void get_ip_address(struct ifreq *if_idx, char *interface);

unsigned short csum(unsigned short *buf, int nwords)
{

   unsigned long sum;

   for(sum=0; nwords>0; nwords--)

       sum += *buf++;

   sum = (sum >> 16) + (sum &0xffff);

   sum += (sum >> 16);

   return (unsigned short)(~sum);

}

/******** udp hdr to be filled, dest ethernet addr, 
	  source port no, dest port no, source ip addr, dest ip addr 
*****/
void create_udp_header(udphdr *udph, u_char dest_ether[6],  
				  u_int16_t src_port, u_int16_t dest_port, 
				  u_int8_t src_ip, u_int8_t dest_ip) {
	int i=0;
	struct ifreq my_mac;
	char intf[20];

	strcpy(intf, IF_NAME);

	get_mac_address(&my_mac, intf);
	udph->s_port = src_port;
	udph->d_port = dest_port;
	//udph->s_ip = src_ip;
	//udph->d_ip = dest_ip;
	udph->eth_type = 0xAAAA;
	udph->protocol_type = 0; // UDP = 0 TCP = 1
	for(i=0;i<6;i++) {
		udph->d_host_ether[i] = dest_ether[i];	
		udph->s_host_ether[i] = (u_int8_t)(my_mac.ifr_hwaddr.sa_data)[i];
	}
	
	/* Calculate IP checksum on completed header */
	udph->check = 0;
	udph->check = csum((unsigned short *)(udph), sizeof(udphdr)/2);

}


void create_udp_packet(char *packet, int packet_size, u_int16_t dest_port, 
		       u_int16_t src_port, char data, u_char dest_ether[6]) {
  //char *packet;
	char *payload;
	u_int8_t src_ip, dest_ip;
	udphdr *udph;
	if(packet_size == 0)
		packet_size = MIN_PACKET_SIZE;

	udph =(udphdr *) packet;

	// Set IP. Only 6 bit IPs. So always set values between 0 to 64; Hardcode for now.
	src_ip = 10;
	dest_ip = 20;
	
	create_udp_header(udph, dest_ether, src_port, dest_port, src_ip, dest_ip);
	payload = packet + UDP_HEADER_SIZE;
	int counter = 0;
	while(counter < (packet_size - UDP_HEADER_SIZE)) {
		*payload = data;
		payload++;
		counter++;
	}
}



void create_tcp_header(tcphdr *tcph, u_char dest_ether[6],  
				  u_int16_t dest_port, u_int16_t src_port, 
				  u_int8_t src_ip, u_int8_t dest_ip, u_int8_t pkt_type) {
	int i=0;
	struct ifreq my_mac;
	char intf[20];

	strcpy(intf, IF_NAME);
	get_mac_address(&my_mac, intf);

	tcph->s_port = src_port;
	tcph->d_port = dest_port;
	//tcph->s_ip = src_ip;
	//tcph->d_ip = dest_ip;
	tcph->eth_type = 0xAAAA;
	tcph->protocol_type = 1; // UDP = 0 TCP = 1
	tcph->packet_type = pkt_type; 

	for(i=0;i<6;i++) {
		tcph->d_host_ether[i] = dest_ether[i];
		tcph->s_host_ether[i] = (my_mac.ifr_hwaddr.sa_data)[i];
	}

	
	/* Calculate IP checksum on completed header */
	tcph->check = 0;
	tcph->check = csum((unsigned short *)(tcph), sizeof(tcphdr)/2);

}


void create_tcp_packet(char *packet, int packet_size, u_int16_t dest_port, 
		       u_int16_t src_port, char data, u_int8_t pkt_type, 
		       u_char dest_ether[6]) {
	//char *packet;
	char *payload;
	//u_char dest_ether[6];
	u_int8_t src_ip, dest_ip;
	tcphdr *tcph;
	if(packet_size == 0)
		packet_size = MIN_PACKET_SIZE;

	tcph =(tcphdr *) packet;
	
	tcph->last_packet= 0;

	// Set IP. Only 6 bit IPs. So always set values between 0 to 64; Hardcode for now.
	src_ip = 10;
	dest_ip = 20;
	
	create_tcp_header(tcph, dest_ether, dest_port, src_port, src_ip, dest_ip, pkt_type);
	payload = packet + TCP_HEADER_SIZE;
	int counter = 0;
	while(counter < (packet_size - TCP_HEADER_SIZE)) {
		*payload = data;
		payload++;
		counter++;
	}
}
