#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/ether.h>
#include <linux/if_packet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

extern u_char dest_ether[6];

void get_mac_address(struct ifreq * if_mac, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if_mac->ifr_addr.sa_family = AF_INET;
	memset(if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFHWADDR, if_mac) < 0)
		perror("SIOCGIFHWADDR");
	close(fd);
}

void get_ip_address(struct ifreq *if_idx, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(if_idx, 0, sizeof(struct ifreq));
	if_idx->ifr_addr.sa_family = AF_INET;
	strncpy(if_idx->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFADDR, if_idx) < 0)
		perror("SIOCGIFADDR");
	//printf("\n Interface : %s , ifr_name : %s", interface, if_idx->ifr_name);
	close(fd);
}

void get_ip_address_index(struct ifreq *if_idx, char *interface) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(if_idx, 0, sizeof(struct ifreq));
	//if_idx->ifr_addr.sa_family = AF_INET;
	strncpy(if_idx->ifr_name, interface, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFINDEX, if_idx) < 0)
		perror("SIOCGIFINDEX");
	close(fd);	
}

void find_peer_and_set_mac() {
	struct ifreq my_mac;
	char interface[20];
	strcpy(interface, "inf000");
	get_mac_address(&my_mac, interface);
	
	if((u_int8_t)(my_mac.ifr_hwaddr.sa_data)[5] == 0x04) {
		// Peer is nodeB
		dest_ether[0] = 0x00;
		dest_ether[1] = 0x00;
		dest_ether[2] = 0x00;
		dest_ether[3] = 0x00;
		dest_ether[4] = 0x00;
		dest_ether[5] = 0x03;

	} else {
		// Peer is nodeA
		dest_ether[0] = 0x00;
		dest_ether[1] = 0x00;
		dest_ether[2] = 0x00;
		dest_ether[3] = 0x00;
		dest_ether[4] = 0x00;
		dest_ether[5] = 0x04;

	}
}

void set_peer_mac(int host) {
	
	switch (host)
	{
		case 1: 
				// Peer is host1
				dest_ether[0] = 0x00;
				dest_ether[1] = 0x00;
				dest_ether[2] = 0x00;
				dest_ether[3] = 0x00;
				dest_ether[4] = 0x00;
				dest_ether[5] = 0x04;
				break;
		case 2:
				// Peer is host2
				dest_ether[0] = 0x00;
				dest_ether[1] = 0x00;
				dest_ether[2] = 0x00;
				dest_ether[3] = 0x00;
				dest_ether[4] = 0x00;
				dest_ether[5] = 0x01;
				break;
		case 3:
				// Peer is host3
				dest_ether[0] = 0x00;
				dest_ether[1] = 0x00;
				dest_ether[2] = 0x00;
				dest_ether[3] = 0x00;
				dest_ether[4] = 0x00;
				dest_ether[5] = 0x03;
				break;
	
	
	}
}

