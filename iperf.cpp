#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "header.h"
#include <unistd.h>
#include <sys/poll.h>
#include <sys/time.h>

extern void create_udp_packet(char *pkt, int packet_size, u_int16_t dest_port, 
			      u_int16_t src_port, char data, u_char dest_ether[6]);
extern void create_tcp_packet(char *pkt, int packet_size, u_int16_t dest_port, 
			      u_int16_t src_port, char data, u_int8_t pkt_type,
			      u_char dest_ether[6]);

extern void send_the_packet(char * pkt, int packet_size);
extern int raw_socket_send_setup();
extern void raw_socket_recv_setup();
extern void set_socket_timeout();
extern int recv_packet(int packet_size, char *pkt);
extern void close_raw_socket();
extern void set_peer_mac(int);

long getinstanttime()
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return((tv.tv_sec*1000000) + tv.tv_usec);
}


#define TOTAL_SIZE 8000
#define server_port_tcp 23456
#define client_port_tcp 23457
#define server_port_udp 23458
#define client_port_udp 23459

#define NODEA 1
#define NODEB 2
#define NODEC 3
#define SERVER_MODE 0
#define CLIENT_MODE 1

u_int16_t server_port, client_port;

u_char dest_ether[6];

extern void find_peer_and_set_mac();

void detect_lost_packets(long lost_packets[], char *seq_check){
	int top=0;
	for(int i=1 ; i<= WINDOW_SIZE; i++){
		if(seq_check[i]==0){	
			lost_packets[top++] = i;
		}
	}
}

void create_nack_queue(char *nack_pkt , long nack_queue[] , int *top){
	char *payload;
	int i = 0;
	payload = nack_pkt + TCP_HEADER_SIZE;
	while(*payload != '\0'){
		nack_queue[i++] = (int)(*payload); 
		payload++;
	}
	*top = i;
	nack_queue[i] = -1;
}

void send_tcp_dummy() {
	char *send_pkt;
	tcphdr *send_tcph;
	u_char dummy_ether[6];

	dummy_ether[0] = 0x00;
	dummy_ether[1] = 0x00;
	dummy_ether[2] = 0x00;
	dummy_ether[3] = 0x00;
	dummy_ether[4] = 0x00;
	dummy_ether[5] = 0x11;
	send_pkt = (char *)malloc(200);
	send_tcph = (tcphdr *) send_pkt;
	create_tcp_packet(send_pkt,200,8888,9999, 'a', SEQ, dest_ether);
	send_the_packet(send_pkt, 200);
}


void create_and_send_tcp(int packet_size, int peer) {
	long num_packets = 0;
	int sflag=0;
	tcphdr *send_tcph, *nack_tcph;
	char *send_pkt, *nack_pkt;
	long nack_queue[WINDOW_SIZE];
	long start_time, end_time, mid_time;
	long retransmitted_packets = 0;

	int next_send_tw = 1;
	int exp_recv_tw = 1;
	
	int window = 0;
	
	raw_socket_send_setup();
	raw_socket_recv_setup();

	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	
	int top = -1;
	start_time = getinstanttime();

	send_pkt = (char *)malloc(packet_size);
	nack_pkt = (char *)malloc(TCP_HEADER_SIZE + WINDOW_SIZE);
	send_tcph = (tcphdr *) send_pkt;
	nack_tcph = (tcphdr *) nack_pkt;

	//set_destination_mac(dest_ether, peer);
	//find_peer_and_set_mac();
	set_peer_mac(peer);
	
	printf("\nTCP Packets");
	printf("\nSending....");
	create_tcp_packet(send_pkt,packet_size,server_port,client_port, 'a', SEQ, dest_ether);
	int itr=1;
	while(1){	
		//Added

		if(top != -1){
			send_tcph->toggle_window = next_send_tw;
			for(int i = 0; i < top-1 ; i++){
				send_tcph->seq_no = nack_queue[i];
				//printf("\nResending seq no: %d",send_tcph->seq_no);
				send_the_packet(send_pkt, packet_size);	
				retransmitted_packets++;
			}
			
			//Send the last packets multiple times

			for(int i = 0 ; i < 5 ; i++){
				send_tcph->seq_no = nack_queue[top-1];
				send_tcph->last_packet = 1;
				send_the_packet(send_pkt, packet_size);	
			}

			next_send_tw = !next_send_tw; // Toggling next_send_tw

			printf("\nRetransmitted %ld packets in the last window",++retransmitted_packets);

			send_tcph->last_packet = 0; // Added this. Else once set all next packets as last packet
			retransmitted_packets = 0;
		}
		else{
	
			send_tcph->toggle_window = next_send_tw;
			int i;
			for (i=1;i<=WINDOW_SIZE-1;i++ ){
				send_tcph->seq_no = i;
				printf("\nSending seq no: %d",send_tcph->seq_no);
				send_the_packet(send_pkt, packet_size);	
				num_packets++;
			}
			for(int j = 0 ; j < 5 ; j++){
				send_tcph->seq_no = i;
				printf("\nSending last packet seq no: %d", send_tcph->seq_no);
				send_tcph->last_packet = 1;
				send_the_packet(send_pkt, packet_size);	
			}
			num_packets++;

			next_send_tw = !next_send_tw; // Toggling next_send_tw

			printf("\nWindow num: %d", ++window);
			send_tcph->last_packet = 0; // Added this. Else once set all next packets as last packet
			
		}

		//Sends the window and waits for an ACK

		int got_ack = 1;
		while(got_ack) {
			memset(nack_pkt, 0, WINDOW_SIZE + TCP_HEADER_SIZE);
			recv_packet(WINDOW_SIZE + TCP_HEADER_SIZE, nack_pkt);

			if(nack_tcph->d_port == client_port) {

				if(nack_tcph->toggle_window != exp_recv_tw) 
					continue;

				printf("\nReceived one ACK for previous window with seq_num : %d", nack_tcph->seq_no);
				got_ack = 0;
				exp_recv_tw = !exp_recv_tw;
			}
			else {
				//printf("\n Some junk we got  ---  ");
				//for(int j=0;j<6;j++)
					//printf("%x:", nack_tcph->d_host_ether[j]);
			}
		}


		//Added

		char *payload;
		payload = nack_pkt + TCP_HEADER_SIZE;
		//printf("\nPayload : %d %d %d %d", payload[0], payload[1], payload[2], payload[3]);
		//printf("\nPayload : %d %d %d %d", payload[0], payload[1], payload[2], payload[3]);
		if((nack_tcph->packet_type == ACK) && *payload=='\0'){
			top = -1;
	       		mid_time = getinstanttime();
			//printf("\n No PACKETS DROPPED IN THIS WINDOW \n %d %d %d %d", payload[0], payload[1], payload[2], payload[3]);	
			if((mid_time-start_time)> 10000000){
				sflag =1;
	 			break;	
			} 
			continue;
		}

		//Process nack packet
		memset(nack_queue , -1, WINDOW_SIZE);
		create_nack_queue(nack_pkt , nack_queue , &top);
				

	}
	send_tcph->toggle_window = next_send_tw;
	create_tcp_packet(send_pkt, packet_size,server_port,client_port, 'b',SEQ, dest_ether);
	for( int i =0 ; i<5 ; i++){
		send_the_packet(send_pkt, packet_size);	
		//printf("\n sent last packet ");
	}
	printf("\n\nTotal Packets Sent %ld", num_packets);
	end_time = getinstanttime();
	free(send_pkt);
	free(nack_pkt);
	double total_sec = (double)(end_time - start_time);
	long throughput = num_packets * packet_size * 8;
	printf("\nThroughput : %f",(double)throughput/total_sec);
	

	// Calculate num of packets.
	// Call create_udp_packet and send_on_raw_socket those many times.
	// Calculate stats
}

void recv_tcp(int packet_size, int peer) {

	long num_packets = 0;
	int flag = 1;
	long recv_num = 0;
	long start_time, end_time;
	tcphdr *send_tcph, *recv_tcph;
	char *seq_check; 
	char * payload;
	char *recv_pkt, *send_pkt;	
	long lost_packets[WINDOW_SIZE];
	int first_packet=0,i=0;
	int next_send_tw = 1;
	int exp_recv_tw = 1;
	int window = 0;
	int itr=0;

	// poll part
	//struct pollfd ufds[1];

	seq_check=(char*) malloc(WINDOW_SIZE);
	memset(seq_check, 0, WINDOW_SIZE);

	raw_socket_recv_setup();
	raw_socket_send_setup();

	set_socket_timeout();

	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	send_tcp_dummy();
	
	int total_nack_size = TCP_HEADER_SIZE + WINDOW_SIZE;
	int ret_recv;
	
	recv_pkt = (char *) malloc(packet_size);
	send_pkt = (char *) malloc(total_nack_size);
	send_tcph = (tcphdr*) send_pkt;	
	recv_tcph = (tcphdr*) recv_pkt;			

	//set_destination_mac(dest_ether, peer);
	//find_peer_and_set_mac();

	//recv_packet(packet_size , recv_pkt);
	//seq_check[recv_tcph->seq_no]=1;

	start_time = getinstanttime();

	printf("\nTCP Packets");
	//printf("\nReceiving....");
	while(1){
		memset(recv_pkt, 0, packet_size);

		//printf("\nReceiving....");
		ret_recv = recv_packet(packet_size , recv_pkt);
		if(ret_recv == 1)
			goto lost_some_packets;
		
		if(recv_tcph->d_port != server_port)
			continue;
		if(recv_tcph->toggle_window != exp_recv_tw) 
			continue;

		payload = recv_pkt + TCP_HEADER_SIZE;
		if(*payload == 'b') {
			flag = 0;
			break;
		}
		if(first_packet == 0)
		{
			for(i=0;i<6;i++)
				dest_ether[i] = recv_tcph->s_host_ether[i];
			//set_peer_mac();
			first_packet=1;
		}	
		printf("\nReceived seq no: %d last packet : %d", recv_tcph->seq_no, recv_tcph->last_packet);
		seq_check[recv_tcph->seq_no]=1;
		recv_num++;
		if(recv_tcph->last_packet == 1){
		lost_some_packets:
			exp_recv_tw = !exp_recv_tw;
			create_tcp_packet(send_pkt, packet_size, client_port, server_port, 'a', ACK , dest_ether);

			
			//Create the array of lost packets
			memset(lost_packets, -1, WINDOW_SIZE);
			detect_lost_packets(lost_packets, seq_check);
			payload = send_pkt + TCP_HEADER_SIZE;

			int i = 0 ;
			while(lost_packets[i] != -1) {
				*payload = lost_packets[i];
				payload++;
				i++;
			}
			*payload = '\0';

			send_tcph->toggle_window = next_send_tw;

			//Sending nack 5 times
			send_tcph->seq_no = itr++;
			for(int j = 0 ; j< 5 ; j++){
				send_the_packet(send_pkt, total_nack_size);	
			}
			
			if(lost_packets[0] == -1)
				printf("\nSending ACK %d with seq_num : %d", i, send_tcph->seq_no);
			else
				printf("\nSending NACK %d with seq_num : %d", i, send_tcph->seq_no);
			
			

			next_send_tw = !next_send_tw;

			if(lost_packets[0] == -1) {
				printf("\nReceived one window!!! num : %d", ++window);
				memset(seq_check, 0, WINDOW_SIZE);
			}
			// Comment this for previous implementation
			memset(send_pkt, 0, WINDOW_SIZE + TCP_HEADER_SIZE);
		}
		
	}
	printf("\n \n Total Received Packets :  %ld", window*WINDOW_SIZE);
	end_time = getinstanttime();
	//free(send_pkt);
	//free(recv_pkt);
	double total_sec = (double)(end_time - start_time);
	long throughput = (recv_num * packet_size * 8);
	printf("\n\nThroughput : %f",(double)throughput/total_sec);
	
	close_raw_socket();	
	// Recv num of packets.
	// Add some kind of timer so that if num_packets is not recvd we come out
	// and calculate stats.
}


void send_udp_dummy() {
	char *send_pkt;
	udphdr *send_udph;
	u_char dummy_ether[6];

	dummy_ether[0] = 0x00;
	dummy_ether[1] = 0x00;
	dummy_ether[2] = 0x00;
	dummy_ether[3] = 0x00;
	dummy_ether[4] = 0x00;
	dummy_ether[5] = 0x11;
	send_pkt = (char *)malloc(200);
	send_udph = (udphdr *) send_pkt;
	create_udp_packet(send_pkt, 200,8888,9999, 'a', dest_ether);
	send_the_packet(send_pkt, 200);	
}

void create_and_send_udp(int packet_size, int peer) {
	long num_packets = 0;
	char *pkt;
	long start_time, end_time, mid_time;

	//find_peer_and_set_mac();
	set_peer_mac(peer);
	
	raw_socket_send_setup();

	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();


	start_time = getinstanttime();
	pkt = (char *)malloc(packet_size);
	create_udp_packet(pkt, packet_size,server_port,client_port, 'a', dest_ether);
	while(1){
		send_the_packet(pkt, packet_size);	
	        mid_time = getinstanttime();
		num_packets++;
		if((mid_time-start_time)> 10000000){
			break;	
		} 	
	}
	printf("\nUDP packets to mac address %d", peer);
	printf("\nSending...");
	create_udp_packet(pkt, packet_size,server_port,client_port, 'b', dest_ether);
	end_time = getinstanttime();
	usleep(100);
	for( int i =0 ; i<5 ; i++){
		send_the_packet(pkt, packet_size);	
		//printf("\n sent last packet ");
	}
	//printf("\n\n Totak Packets sent %d", num_packets);
		free(pkt);
	double total_sec = (double)(end_time - start_time);
	long long throughput = (num_packets * packet_size * 8);
	//printf("\n\nThroughput : %f",(double)throughput/total_sec);

	
	// Calculate num of packets.
	// Call create_udp_packet and send_on_raw_socket those many times.
	// Calculate stats
}

void recv_udp(int packet_size) {
	int num_packets = TOTAL_SIZE / packet_size;
	int flag = 1;
	long recv_num = 0;
	long start_time, end_time, mid_time;
	udphdr *udph;
	char * payload;
	char *pkt;
	int recv_ret;
	
	//find_peer_and_set_mac();
	raw_socket_recv_setup();
	raw_socket_send_setup();

	set_socket_timeout();

	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();
	send_udp_dummy();


	pkt = (char *) malloc(packet_size);
	recv_packet(packet_size , pkt);
	start_time = getinstanttime();
	printf("\nUDP Packets");
	printf("\nReceiving...");
	while(1){
		recv_ret = recv_packet(packet_size , pkt);
		if(recv_ret == 1)
			continue;
		udph = (udphdr *)pkt;
		if(udph->d_port != server_port)
			continue;

		//printf("\nReceived packet");
	        mid_time = getinstanttime();
		if((mid_time-start_time)> 10000000)
			break;	
		recv_num++;
		//payload = pkt + UDP_HEADER_SIZE;
		/*
		if(*payload == 'b') {
			flag =0;
			printf("\n\n received last packet");
			break;
			}*/
		//if( flag == 0) break;
	}
	printf("\n Total Received Packets  : %ld", recv_num);
	end_time = getinstanttime();
	free(pkt);
	double total_sec = (double)(end_time - start_time);
	long throughput = (recv_num * packet_size * 8);
	printf("\n\nThroughput : %f",(double)throughput/total_sec);
	
	close_raw_socket();	
	// Recv num of packets.
	// Add some kind of timer so that if num_packets is not recvd we come out
	// and calculate stats.
}

void start_udp(int mode,int packet_size,char* server_name){
        printf("UDP mode, with packet size = %d bytes\n",packet_size );
	if(mode==0) {
		printf("\nCalling recv");
		
		recv_udp(packet_size);
	}
	else {
		if(strcmp(server_name, "host1") == 0)
			create_and_send_udp(packet_size, 1);
		else if(strcmp(server_name, "host2") == 0)
			create_and_send_udp(packet_size, 2);
		else
			create_and_send_udp(packet_size, 3);
	}
}

void start_tcp(int mode,int packet_size,char* server_name){
        printf("TCP mode, with packet size = %d bytes\n", packet_size );
	if(mode==SERVER_MODE)
			recv_tcp(packet_size, NODEA);
			
	else {
		if(strcmp(server_name, "host1") == 0)
			create_and_send_tcp(packet_size, 1);
		else if(strcmp(server_name, "host2") == 0)
			create_and_send_tcp(packet_size, 2);
		else
			create_and_send_tcp(packet_size, 3);
			
	}
}

void usage(void)   
{
        printf("\nUSAGE: iperf558 [-u udpmode] [-sc server/client] [-l packetsize]\n");
        exit(-1);
}

int main(int argc, char *argv[])
{
	int tcp_udp_mode = 0;                   // default mode tcp
	int server_client_mode = -1;            
	int packet_size = 1470;
	//char* server_name = NULL;
	char server_name[20];
	
	while(argc > 1) {
		if (strcmp(argv[1],"-u")==0){
			tcp_udp_mode = 1;
		}else if (strcmp(argv[1],"-s")==0){
			server_client_mode = SERVER_MODE;
		}else if (strcmp(argv[1],"-c")==0){
			if(argv[2][0] == '-') 
				usage();
			server_client_mode = CLIENT_MODE;
			//server_name = argv[2]; 
			strcpy(server_name, argv[2]);
			++argv;--argc;
		}else if (strcmp(argv[1],"-l")==0){
			if(argv[2][0] == '-') 
				usage();
			packet_size = atoi(argv[2]);
			++argv;--argc;
		}else if (strcmp(argv[1],"-o")==0){
			if(argv[2][0] == '-') 
				usage();
			client_port = atoi(argv[2]);
			++argv;--argc;
		}else if (strcmp(argv[1],"-i")==0){
			if(argv[2][0] == '-') 
				usage();
			server_port = atoi(argv[2]);
			++argv;--argc;
		}
		else {
			printf("Incorrect arguments supplied\n\n");
			usage();
		}
                ++argv;
		--argc;
        }
	if(server_client_mode < 0)
		usage();
	
	if(tcp_udp_mode > 0)
		start_udp(server_client_mode,packet_size,server_name);
	else
		start_tcp(server_client_mode,packet_size,server_name);
	
	return 0;
}
