#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/ether.h>
#include <linux/if_packet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define IF_NAME "inf000"

int tx_len = 0;
char sendbuf[1024];
char source_ip[16], dest_ip[16];
struct sockaddr_ll socket_address_1;
int socknew_1, socknew_2;

extern void get_ip_address_index(struct ifreq *if_idx, char *interface);
extern void get_mac_address(struct ifreq * if_mac, char *interface);
extern void get_ip_address(struct ifreq *if_idx, char *interface);

void send_the_packet(char *pkt, int pkt_len) {
	/* Send packet */
	int i;
	if (sendto(socknew_1, pkt, pkt_len, 0, (struct sockaddr*)&socket_address_1, sizeof(struct sockaddr_ll)) < 0)
		i=0;//printf("Send failed %s\n ", strerror(errno));
}

int raw_socket_send_setup() {

	struct ether_header *eh;
	struct ifreq if_mac_1, if_ip_1, if_idx_1;
	char dest_ip_addr[20], *interface;
	char ip_interface1[20], interface1[20];
	
	strcpy(interface1, IF_NAME);
	
	get_mac_address(&if_mac_1, interface1);
	get_ip_address(&if_ip_1, interface1);
	get_ip_address_index(&if_idx_1, interface1);
	
	/* Destination address */
	/* Index of the network device */
	socket_address_1.sll_ifindex = if_idx_1.ifr_ifindex;
	/* Address length*/
	//socket_address_1.sll_halen = ETH_ALEN;
	socket_address_1.sll_family = AF_PACKET;
	socket_address_1.sll_protocol = htons(ETH_P_ALL);  


	/* Destination MAC */
	/*socket_address_1.sll_addr[0] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[0];
	socket_address_1.sll_addr[1] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[1];
	socket_address_1.sll_addr[2] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[2];
	socket_address_1.sll_addr[3] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[3];
	socket_address_1.sll_addr[4] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[4];
	socket_address_1.sll_addr[5] = (u_int8_t)(if_mac_1.ifr_hwaddr.sa_data)[5];
	*/
	
	/* Open RAW socket to send on */
	if ((socknew_1 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1) {
		perror("socket");
	}
	
	int res, sendbuff = 300000;

        //printf("sets the send buffer to %d\n", sendbuff);
        //res = setsockopt(socknew_1, SOL_SOCKET, SO_SNDBUF, &sendbuff, sizeof(sendbuff));
}

